import psycopg2
import psycopg2.extras

conn = psycopg2.connect(
    database="streaming",
    host="localhost",
    user="romanchechetkin",
    password="5432",
    port="5432"
)

conn.autocommit = True

def create_table():
    """Cheking if database exists, if not, create one.
    """
    with conn.cursor() as cursor:
        cursor.execute('''SELECT EXISTS (
                      SELECT 1
                      FROM information_schema.tables
                      WHERE table_schema = 'public' AND
                            table_type = 'BASE TABLE' AND
                            table_name = 'users'
                    );''')
        if not cursor.fetchone()[0]:
            cursor.execute(''' CREATE TYPE roles AS ENUM('admin','user');''')
            cursor.execute('''CREATE TABLE users (
                  id serial PRIMARY KEY,
                  email VARCHAR(255),
                  password VARCHAR(255),
                  role roles
                  );''')
            conn.commit()


def add_user(email, password, role="user") -> int:
    """Adding user to database.

    Args:
        email (_type_): User email (email).
        password (_type_): User password (Hash).
        role (str, optional): User role (user or admin) . Defaults to "user".

    Returns:
        _type_: If user was added returns True, else False.
    """
    cursor = conn.cursor()
    cursor.execute('''INSERT INTO users (email, password, role)
                   VALUES (%s, %s, %s) RETURNING id''', (email, password, role))
    id = cursor.fetchone()[0]
    if id:
        return int(id)
    return -1


def check_user(email: str) -> bool:
    """Cheking if user in database.

    Args:
        login (_type_): User email (email).

    Returns:
        _type_: Returns True if users already in database, else False.
    """
    cursor = conn.cursor()

    query = f"SELECT 1 FROM users WHERE email LIKE '{email}'"
    cursor.execute(query)

    result = cursor.fetchone()
    if result:
        return True

    return False


def get_password(email) -> str:
    """Getting user password by email.

    Args:
        login (_type_): User email.

    Returns:
        _type_: Returns password in string formate
    """
    cursor = conn.cursor()

    query = f"SELECT password FROM users WHERE email LIKE '{email}'"
    cursor.execute(query)
    password = cursor.fetchone()[0]

    return password if password else ""


def get_user(email):
    """Getting information about user by it's email.

    Args:
        login (_type_): User email.

    Returns:
        _type_: Returns tuple of user information.
    """
    cursor = conn.cursor()

    query = f"SELECT * FROM users WHERE email LIKE '{email}'"

    cursor.execute(query)
    return cursor.fetchall()[0]


def get_user_by_id(id):
    """Getting information about user by it's ID.

    Args:
        id (_type_): User ID.

    Returns:
        _type_: Returns tuple of user information.
    """
    cursor = conn.cursor()

    query = f"SELECT * FROM users WHERE id={id}"

    cursor.execute(query)
    return cursor.fetchall()[0]
