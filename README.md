# Auth Service

# GRPC 
    - rpc Reigistration(RegistrationRequest) returns (RegistrationResponse) {}
    - rpc Login(LoginRequest) returns (LoginResponse) {}
    - rpc RefreshTokens(RefreshTokensRequest) returns (RefreshTokensResponse) {}

```proto
message RegistrationRequest {
    string email = 1;
    string password = 2;
}

message RegistrationResponse {
    int64 user_id = 1;
}

message LoginRequest {
    string email = 1;
    string password = 2;
}

message LoginResponse {
    string access_token = 1;
    string refresh_token = 2;
}

message RefreshTokensRequest {
    string refresh_token = 1;
}

message RefreshTokensResponse {
    string access_token = 1;
    string refresh_token = 2;
}
```    