import datetime
import hashlib
import logging
from concurrent import futures
from datetime import timedelta

import grpc
import jwt

import pb.auth.auth_pb2 as auth_pb2
import pb.auth.auth_pb2_grpc as auth_pb2_grpc
from config import *
from database import *


def create_access_token(user: dict, expires_delta: timedelta | None = None):
    if expires_delta:
        expire = datetime.datetime.utcnow() + expires_delta
    else:
        expire = datetime.datetime.utcnow() + timedelta(minutes=15)

    jwt_data = {
        "sub": str(user["id"]),
        "email": str(user["email"]),
        "role": str(user["role"]),
        "exp": expire,
    }
    
    encoded_data = jwt.encode(jwt_data, ACCESS_SECRET_KEY, algorithm=ALGORITHM)

    return encoded_data



def create_refresh_token(user: dict, expires_delta: timedelta | None = None):
    if expires_delta:
        expire = datetime.datetime.utcnow() + expires_delta
    else:
        expire = datetime.datetime.utcnow() + timedelta(days=1)

    jwt_data = {
        "sub": str(user["id"]),
        "exp": expire,
    }

    encoded_data = jwt.encode(jwt_data, REFRESH_SECRET_KEY, algorithm=ALGORITHM)

    return encoded_data

class AuthorizationServicer(auth_pb2_grpc.AuthorizationServicer):
    def __init__(self, log: logging.Logger) -> None:
        self.log = log

    def Registration(self, request: auth_pb2.RegistrationRequest, context: grpc.ServicerContext):
        self.log.info("registration rpc call; email=%s; password=%s", request.email, request.password)

        if check_user(request.email):
                return auth_pb2.RegistrationResponse(user_id=-1)
            
        password = request.password.encode('utf-8')
        hash_object = hashlib.sha256(password)

        user_id = add_user(request.email, hash_object.hexdigest())

        return auth_pb2.RegistrationResponse(user_id=user_id)

    def Login(self, request: auth_pb2.LoginRequest, context: grpc.ServicerContext):
        self.log.info("login rpc call; email=%s; password=%s", request.email, request.password)

        if not check_user(request.email):
                return auth_pb2.RefreshTokensResponse(access_token="", refresh_token="")
            
        password = request.password.encode('utf-8')
        hash_object = hashlib.sha256(password)
        password = hash_object.hexdigest()


        data = get_user(request.email)
        if password != data[2]:
            return auth_pb2.LoginResponse(access_token="", refresh_token="")
           
        access_token_expires = timedelta(
            minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            user={
                "id": data[0],
                "email": data[1],
                "role": data[3]},
            expires_delta=access_token_expires)

        refresh_token_expires = timedelta(
            minutes=REFRESH_TOKEN_EXPIRE_DAYS)
        refresh_token = create_refresh_token(
            user={"id": data[0]}, expires_delta=refresh_token_expires
        )

        return auth_pb2.LoginResponse(access_token=access_token, refresh_token=refresh_token, user={
            "id": data[0],
            "email": data[1],
            "role": data[3],
        })

    def RefreshTokens(self, request: auth_pb2.RefreshTokensRequest, context: grpc.ServicerContext):
        self.log.info("refresh tokens rpc call; refresh token=%s", request.refresh_token)

        refresh = jwt.decode(
            request.refresh_token,
            REFRESH_SECRET_KEY,
            algorithms=ALGORITHM)
        user = get_user_by_id(refresh["sub"])
        access_token_expires = timedelta(
            minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            user={
               "id": user[0],
                "email": user[1],
                "role": user[3]},
            expires_delta=access_token_expires)

        refresh_token_expires = timedelta(
            minutes=REFRESH_TOKEN_EXPIRE_DAYS)
        refresh_token = create_refresh_token(
            user={"id": user[0]}, expires_delta=refresh_token_expires
        )
        
        return auth_pb2.LoginResponse(access_token=access_token, refresh_token=refresh_token)


def serve(log: logging.Logger):
    port = "50051"
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    auth_pb2_grpc.add_AuthorizationServicer_to_server(AuthorizationServicer(log), server)
    server.add_insecure_port("[::]:" + port)
    server.start()
    log.info("Server started, listening on %s", port)
    server.wait_for_termination()


def setupLogger() -> logging.Logger:
    logging.basicConfig(level=logging.INFO)
    return logging.getLogger()

if __name__ == "__main__":
    create_table()
    
    log = setupLogger()
    
    serve(log)
